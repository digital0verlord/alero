import 'dart:convert';
import 'dart:io';

import 'package:alero/models/response/login_response.dart';
import 'package:alero/utils/Pandora.dart';
import 'package:alero/utils/firebase_performance.dart';
import 'package:http/http.dart';
import 'package:http/io_client.dart';

import '../models/customer/CustomerDetailsResponse.dart';
import '../models/customer/DataExceptionResponse.dart';
import '../models/customer/RevenueDataResponse.dart';
import '../models/landing/GetStaffInformation.dart';
import '../models/landing/view_status_response.dart';
import '../models/search/SearchUserResponse.dart';
import '../utils/Global.dart';

class AleroAPIService {
  final ioc = new HttpClient();
  final Pandora pandora = new Pandora();
  String prefToken;

  var preAuthHeaders = {
    "content-type": "application/json",
    "accept": "application/json",
    "AppId": Global.AppId,
    "DeviceIp": Global.DeviceIp,
    "DeviceManufacturer": Global.DeviceManufacturer,
    "DeviceName": Global.DeviceName,
    "DeviceModel": Global.DeviceModel,
    "DeviceType": Global.DeviceType
  };

  var postAuthHeaders = {
    "content-type": "application/json",
    "accept": "application/json",
    "Authorization": "Bearer " + Global.API_TOKEN,
    "AppId": Global.AppId,
    "DeviceIp": Global.DeviceIp,
    "DeviceManufacturer": Global.DeviceManufacturer,
    "DeviceModel": Global.DeviceModel,
    "DeviceName": Global.DeviceName,
    "DeviceType": Global.DeviceType
  };

  /// Login user with email and password
  Future<LoginResponse> loginUser(String email, String password) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
    try {
      var response = await http.post(Global.BaseUrl + '/token',
          headers: preAuthHeaders,
          body: json.encode({"username": email, "password": password}));
      if (response.statusCode == 200) {
        return LoginResponse.fromJson(jsonDecode(response.body));
      } else if (response.statusCode == 401) {
        pandora.logFirebaseEvent('LOGIN', '/token', response.body);
        throw Exception(response.body);
      } else {
        pandora.logFirebaseEvent('LOGIN', '/token', response.body);
        print('failed to reach server');
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent('LOGIN', '/token', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent('LOGIN', '/token', error.toString());
      throw Exception(error);
    }
  }

  Future<dynamic> logoutUser() async {
    print('Logging out User ');
    print(Global.PREF_TOKEN);
    var logOutHeaders = {
      "content-type": "application/json",
      "accept": "application/json",
      "Authorization": 'Bearer ' + Global.PREF_TOKEN,
      "AppId": Global.AppId,
      "DeviceIp": Global.DeviceIp,
      "DeviceManufacturer": Global.DeviceManufacturer,
      "DeviceName": Global.DeviceName,
      "DeviceModel": Global.DeviceModel,
      "DeviceType": Global.DeviceType
    };
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
    try {
      var response = await http.get(
        Global.BaseUrl + '/aleroUserMgt/logout',
        headers: logOutHeaders,
      );
      if (response.statusCode == 200) {
        return response;
      } else if (response.statusCode == 401) {
        pandora.logFirebaseEvent(
            'LOGOUT', '/aleroUserMgt/logout', response.body);
        throw Exception(response.body);
      } else {
        pandora.logFirebaseEvent(
            'LOGOUT', '/aleroUserMgt/logout', response.body);
        print('failed to reach server');
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent(
          'LOGOUT', '/aleroUserMgt/logout', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent(
          'LOGOUT', '/aleroUserMgt/logout', error.toString());
      throw Exception(error);
    }
  }

  /// Check if user is RM
  Future<ViewStatusResponse> getUserStatus() async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      var response = await http.get(Global.BaseUrl + '/view-status-val',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        return ViewStatusResponse.fromJson(jsonDecode(response.body));
      } else {
        pandora.logFirebaseEvent(
            'GET_USER_STATUS', '/view-status-val', response.body);
        throw Exception('Failed to load RM Status');
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent(
          'GET_USER_STATUS', '/view-status-val', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent(
          'GET_USER_STATUS', '/view-status-val', error.toString());
      throw Exception(error);
    }
  }

  /// Get staff details
  Future<GetStaffInformation> getStaffInformation() async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      var response = await http.get(Global.BaseUrl + '/userName',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        return GetStaffInformation.fromJson(jsonDecode(response.body));
      } else {
        pandora.logFirebaseEvent(
            'GET_STAFF_INFORMATION', '/userName', response.body);
        throw Exception('Failed to load staff details');
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent(
          'GET_STAFF_INFORMATION', '/userName', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent(
          'GET_STAFF_INFORMATION', '/userName', error.toString());
      throw Exception(error);
    }
  }

  /// Search for Customer
  Future<List<dynamic>> searchForUser(String searchQuery) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    print(Global.API_TOKEN);
    final http = new IOClient(ioc);
    try {
      var response = await http.get(
          Global.BaseUrl + '/customer-details/$searchQuery',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          List<dynamic> myResponse = [];
          var staffInfo =
              SearchUserResponse.fromJson(jsonDecode(response.body));
          Map<String, dynamic> item = {
            'groupId': staffInfo.groupId,
            'customerId': staffInfo.customerId,
            'customerName': staffInfo.customerName,
            'depositBalance': staffInfo.depositBalance,
            'loanBalance': staffInfo.loanBalance,
            'ytdRevenue': staffInfo.ytdRevenue,
            'prevMonthRevenue': staffInfo.prevMonthRevenue,
            'customerRelationshipAge': staffInfo.customerRelationshipAge,
            'businessSegment': staffInfo.businessSegment,
            'subBusinessSegment': staffInfo.subBusinessSegment,
            'rmCode': staffInfo.rmCode,
            'rmName': staffInfo.rmName,
            'activeStat': staffInfo.activeStat,
          };
          myResponse.add(item);
          return (myResponse);
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent(
            'SEARCH_FOR_USER', '/customer-details/$searchQuery', response.body);
        return [];
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent('SEARCH_FOR_USER',
          '/customer-details/$searchQuery', exception.toString());
      return [];
    } catch (error) {
      pandora.logFirebaseEvent('SEARCH_FOR_USER',
          '/customer-details/$searchQuery', error.toString());
      return [];
    }
  }

  /// Get customer details
  Future<CustomerDetailsResponse> getCustomerDetails(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      var response = await http.get(Global.BaseUrl + '/customer-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        return CustomerDetailsResponse.fromJson(jsonDecode(response.body));
      } else {
        pandora.logFirebaseEvent(
            'GET_CUSTOMER_DETAILS', '/customer-data/$groupId', response.body);
        throw Exception('Failed to load customer details');
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent('GET_CUSTOMER_DETAILS',
          '/customer-data/$groupId', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent(
          'GET_CUSTOMER_DETAILS', '/customer-data/$groupId', error.toString());
      throw Exception(error);
    }
  }

  /// Get customer data exceptions
  Future<DataExceptionResponse> getCustomerDataExceptions(
      String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    try {
      var response = await http.get(
          Global.BaseUrl + '/customer-data-exceptions/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        return DataExceptionResponse.fromJson(jsonDecode(response.body));
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent(
            'GET_CUSTOMER_DETAILS', '/customer-data/$groupId', response.body);
        throw Exception(ExceptionTypes.NODATAEXCEPTION);
      } else {
        pandora.logFirebaseEvent(
            'GET_CUSTOMER_DETAILS', '/customer-data/$groupId', response.body);
        throw Exception(ExceptionTypes.UNKNOWN);
      }
    } on Exception catch (exception) {
      pandora.logFirebaseEvent('GET_CUSTOMER_DATA_EXCEPTION',
          '/customer-data-exceptions/$groupId', exception.toString());
      throw Exception(exception);
    } catch (error) {
      pandora.logFirebaseEvent('GET_CUSTOMER_DATA_EXCEPTION',
          '/customer-data-exceptions/$groupId', error.toString());
      throw Exception(error);
    }
  }

  /// Get customer banking data
  Future<List<dynamic>> getBankingData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

      var response = await http.get(
          Global.BaseUrl + '/customer-banking-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          print('Error 1');
          pandora.logFirebaseEvent('GET_CUSTOMER_BANKING_DATA',
              '/customer-banking-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        print('Error 2');
        pandora.logFirebaseEvent('GET_CUSTOMER_BANKING_DATA',
            '/customer-banking-data/$groupId', response.body);
        return [];
      } else {
        print('Error 3');
        pandora.logFirebaseEvent('GET_CUSTOMER_BANKING_DATA',
            '/customer-banking-data/$groupId', response.body);
        return [];
      }
  }

  /// Get revenue data
  Future<RevenueDataResponse> getRevenueData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

      var response = await http.get(
          Global.BaseUrl + '/customer-revenue-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        return RevenueDataResponse.fromJson(jsonDecode(response.body));
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_DATA_EXCEPTION',
            '/customer-data-exceptions/$groupId', response.body);
        throw Exception('Failed to load customer revenue data');
      }
  }

  /// Get investment data
  Future<List<dynamic>> getCustomerInvestmentData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-investment-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_INVESTMENT_DATA',
              '/customer-investment-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_INVESTMENT_DATA',
            '/customer-investment-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_INVESTMENT_DATA',
            '/customer-investment-data/$groupId', response.body);
        return [];
      }
  }

  /// Get PND data
  Future<List<dynamic>> getPNDData(String groupId) async {
    print("here");
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-pnd-details/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent(
              'GET_PND_DATA', '/customer-pnd-details/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent(
            'GET_PND_DATA', '/customer-pnd-details/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent(
            'GET_PND_DATA', '/customer-pnd-details/$groupId', response.body);
        return [];
      }
  }

  /// Get debit card data
  Future<List<dynamic>> getDebitCardData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-debitcard-details/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_DEBIT_CARD_DATA',
              '/customer-debitcard-details/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_DEBIT_CARD_DATA',
            '/customer-debitcard-details/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_DEBIT_CARD_DATA',
            '/customer-debitcard-details/$groupId', response.body);
        return [];
      }
  }

  ///Get Channels Data
  Future<List<dynamic>> getChannelsData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-channel-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CHANNEL_DATA',
              '/customer-channel-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CHANNEL_DATA',
            '/customer-channel-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CHANNEL_DATA',
            '/customer-channel-data/$groupId', response.body);
        return [];
      }
  }

  /// Get value chain data
  Future<List<dynamic>> getValueChainData(String customerId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-value-chain/$customerId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_VALUE_CHAIN_DATA',
              '/customer-value-chain/$customerId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_VALUE_CHAIN_DATA',
            '/customer-value-chain/$customerId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_VALUE_CHAIN_DATA',
            '/customer-value-chain/$customerId', response.body);
        return [];
      }
  }

  /// Get customer loans data
  Future<List<dynamic>> getCustomerLoansData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-loan-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_LOANS',
              '/customer-loan-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_LOANS',
            '/customer-loan-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_LOANS',
            '/customer-loan-data/$groupId', response.body);
        return [];
      }
  }

  /// Volume:  Get lifestyle count data
  Future<List<dynamic>> getLifeStyleCountData(String customerId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-lifestyle-count-data/$customerId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_LIFESTYLE_DATA',
              '/customer-lifestyle-count-data/$customerId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_LIFESTYLE_DATA',
            '/customer-lifestyle-count-data/$customerId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_LIFESTYLE_DATA',
            '/customer-lifestyle-count-data/$customerId', response.body);
        return [];
      }
  }

  /// Volume:  Get lifestyle value data
  Future<List<dynamic>> getLifeStyleValueData(String customerId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-lifestyle-value-data/$customerId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_LIFESTYLE_VALUE_DATA',
              '/customer-lifestyle-value-data/$customerId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_LIFESTYLE_VALUE_DATA',
            '/customer-lifestyle-value-data/$customerId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_LIFESTYLE_VALUE_DATA',
            '/customer-lifestyle-value-data/$customerId', response.body);
        return [];
      }
  }

  ///Get customer touch point data
  Future<List<dynamic>> getTouchPointData(String groupId) async {
    print("loading touch points");
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-touch-point/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_TOUCH_POINT_DATA',
              '/customer-touch-point/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_TOUCH_POINT_DATA',
            '/customer-touch-point/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_TOUCH_POINT_DATA',
            '/customer-touch-point/$groupId', response.body);
        return [];
      }
  }

  ///Get NBO Data
  Future<List<dynamic>> getNBOData(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-nbo-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent(
              'GET_NBO_DATA', '/customer-nbo-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent(
            'GET_NBO_DATA', '/customer-nbo-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent(
            'GET_NBO_DATA', '/customer-nbo-data/$groupId', response.body);
        return [];
      }
  }

  ///Get total inflow
  Future<List<dynamic>> getTransactionInflow(
      String customerId, int range) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl +
              '/customer-transaction-credit-trend/$customerId/$range',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent(
              'GET_TRANSACTION_INFLOW',
              '/customer-transaction-credit-trend/$customerId/$range',
              response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent(
            'GET_TRANSACTION_INFLOW',
            '/customer-transaction-credit-trend/$customerId/$range',
            response.body);
        return [];
      } else {
        pandora.logFirebaseEvent(
            'GET_TRANSACTION_INFLOW',
            '/customer-transaction-credit-trend/$customerId/$range',
            response.body);
        return [];
      }
  }

  /// Get customer outflow
  Future<List<dynamic>> getTransactionOutflow(
      String customerId, int range) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-transaction-trend/$customerId/$range',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_TRANSACTION_OUTFLOW',
              '/customer-transaction-trend/$customerId/$range', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_TRANSACTION_OUTFLOW',
            '/customer-transaction-trend/$customerId/$range', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_TRANSACTION_OUTFLOW',
            '/customer-transaction-trend/$customerId/$range', response.body);
        return [];
      }
  }

  /// Get recent transactions
  Future<List<dynamic>> getRecentTransactions(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-recent-transactions/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_RECENT_TRANSACTION',
              '/customer-recent-transactions/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_RECENT_TRANSACTION',
            '/customer-recent-transactions/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_RECENT_TRANSACTION',
            '/customer-recent-transactions/$groupId', response.body);
        return [];
      }
  }

  /// Get customer signatories
  Future<List<dynamic>> getCustomerSignatories(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-signatories-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_SIGNATORIES',
              '/customer-signatories-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_SIGNATORIES',
            '/customer-signatories-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_SIGNATORIES',
            '/customer-signatories-data/$groupId', response.body);
        return [];
      }
  }

  /// Get customer directors
  Future<List<dynamic>> getCustomerDirectors(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-directors-data/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        print(response.body);
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_DIRECTORS',
              '/customer-directors-data/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_DIRECTORS',
            '/customer-directors-data/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_DIRECTORS',
            '/customer-directors-data/$groupId', response.body);
        return [];
      }
  }

  /// Get customer complaints
  Future<List<dynamic>> getCustomerComplaints(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
    //try {
      var response = await http.get(
          Global.BaseUrl + '/customer-complaints/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINTS',
              '/customer-complaints/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINTS',
            '/customer-complaints/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINTS',
            '/customer-complaints/$groupId', response.body);
        return [];
      }

  }

  /// Get customer complaint categories
  Future<List<dynamic>> getComplaintCategories(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-complaint-categories/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        print(response.body);
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_COMPLAINT_CATEGORIES',
              '/customer-complaint-categories/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_COMPLAINT_CATEGORIES',
            '/customer-complaint-categories/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_COMPLAINT_CATEGORIES',
            '/customer-complaint-categories/$groupId', response.body);
        return [];
      }
  }

  /// Get customer complaint trend
  Future<List<dynamic>> getCustomerComplaintTrend(String groupId) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);
      var response = await http.get(
          Global.BaseUrl + '/customer-complaint-trend/$groupId',
          headers: postAuthHeaders);
      if (response.statusCode == 200) {
        print(response.body);
        if (jsonDecode(response.body) is List) {
          return jsonDecode(response.body);
        } else {
          pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINT_TREND',
              '/customer-complaint-trend/$groupId', response.body);
          return [];
        }
      } else if (response.statusCode == 404) {
        pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINT_TREND',
            '/customer-complaint-trend/$groupId', response.body);
        return [];
      } else {
        pandora.logFirebaseEvent('GET_CUSTOMER_COMPLAINT_TREND',
            '/customer-complaint-trend/$groupId', response.body);
        return [];
      }

  }

  Future<void> testHttpMetric(String baseUrl) async {
    ioc.badCertificateCallback =
        (X509Certificate cert, String host, int port) => true;
    final http = new IOClient(ioc);

    final MetricHttpClient metricHttpClient = MetricHttpClient(http);

    final Request request = Request("SEND", Uri.parse(baseUrl));

    metricHttpClient.send(request);
  }
}
