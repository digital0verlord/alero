import 'package:alero/screens/alero/home/home_screen.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';

import 'auth/login_page.dart';
import 'components/route_animator.dart';
import 'customer/customer_details.dart';
import 'error/error_page.dart';
import 'landing/landing_page.dart';
import 'search/search_page.dart';

class Routes {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final arguments = settings.arguments;

    switch (settings.name) {
      case '/login':
        GetIt.I<FirebaseAnalytics>()
            .setCurrentScreen(screenName: 'Login Screen');
        return RouteAnimator(page: LoginPage());
      case '/landing':
        GetIt.I<FirebaseAnalytics>()
            .setCurrentScreen(screenName: 'Landing Page');
        return RouteAnimator(page: HomeScreen(data: arguments));
      case '/single-customer-view':
        GetIt.I<FirebaseAnalytics>()
            .setCurrentScreen(screenName: 'Single Customer View Page');
        return RouteAnimator(
            page: SingleCustomerViewLanding(rmName: arguments));
      case '/search':
        GetIt.I<FirebaseAnalytics>()
            .setCurrentScreen(screenName: 'Search Page');
        return RouteAnimator(page: SearchPage(searchQuery: arguments));
      case '/customer-profile':
        GetIt.I<FirebaseAnalytics>()
            .setCurrentScreen(screenName: 'Customer Profile Page');
        return RouteAnimator(page: CustomerDetails(groupId: arguments));
      default:
        return RouteAnimator(page: ErrorPage());
    }
  }
}
