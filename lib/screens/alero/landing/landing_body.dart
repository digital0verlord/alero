import '../../../network/AleroAPIService.dart';
import '../../../utils/Pandora.dart';
import 'package:eva_icons_flutter/eva_icons_flutter.dart';
import 'package:flutter/material.dart';
import '../../../style/theme.dart' as Style;
import '../../../utils/Strings.dart' as Strings;
import '../components/button.dart';
import 'landing_grid.dart';
import 'landing_search_field.dart';

class LandingBody extends StatefulWidget {
  final String rmName;

  const LandingBody({Key key, @required this.rmName}) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _LandingBodyState();
  }
}

class _LandingBodyState extends State<LandingBody> {
  final Pandora pandora = new Pandora();
  var apiService = AleroAPIService();
  String firstName;
  TextEditingController searchFieldController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (mounted) {
      setState(() {
        firstName = widget.rmName;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final Pandora pandora = new Pandora();
    return SingleChildScrollView(
      physics: BouncingScrollPhysics(),
      child: Column(
        children: [
          Container(
              height: size.height * 0.2,
              width: size.width,
              child: Align(
                child: new Text(
                  "Hello $firstName!",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    color: Style.Colors.blackTextColor,
                    fontSize: 22.0,
                    fontWeight: FontWeight.w700,
                    fontFamily: 'Poppins-Regular',
                  ),
                ),
              )),
          Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                LandingGridItem(
                    topImage: 'assets/landing/landing_graph_outer.svg',
                    topImageVisible: true,
                    image: 'assets/landing/landing_graph_inner.svg',
                    bottomImage: 'assets/landing/landing_graph_outer.svg',
                    bottomImageVisible: false,
                    press: () {}),
                LandingGridItem(
                    topImage: 'assets/landing/landing_graph_outer.svg',
                    topImageVisible: false,
                    image: 'assets/landing/landing_chart_inner.svg',
                    bottomImage: 'assets/landing/landing_chart_outer.svg',
                    bottomImageVisible: true,
                    press: () {}),
              ],
            ),
          ),
          Padding(
            padding: EdgeInsets.all(23.0),
            child: Wrap(
              children: [
                Container(
                  width: size.width,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      Text(
                        "Find A Customer",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Style.Colors.blackTextColor,
                          fontSize: 20.0,
                          fontWeight: FontWeight.w700,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      Text(
                        "Search for any customer bankwide using either their account number,  name or customer’s ID to view comprehensive data on their transaction trend or revenue performance.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Style.Colors.greyTextColor,
                          fontSize: 12.0,
                          fontFamily: 'Poppins-Regular',
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          TextFieldContainer(
            child: TextField(
              autocorrect: false,
              textAlign: TextAlign.start,
              controller: searchFieldController,
              textAlignVertical: TextAlignVertical.center,
              cursorColor: Style.Colors.greyTextColor,
              textInputAction: TextInputAction.search,
              onSubmitted: (value) {
                searchAction(value, context);
              },
              style: TextStyle(
                  color: Style.Colors.blackTextColor,
                  fontSize: 18.0,
                  fontFamily: 'Poppins-Regular',
                  fontWeight: FontWeight.normal),
              decoration: InputDecoration(
                border: InputBorder.none,
                prefixIcon: Icon(
                  EvaIcons.searchOutline,
                  color: Style.Colors.buttonColor,
                ),
              ),
            ),
          ),
          RoundedButton(
              text: "Find",
              color: Style.Colors.buttonColor,
              textColor: Colors.white,
              press: () {
                if (searchFieldController.text.trim().isNotEmpty) {
                  searchAction(searchFieldController.text, context);
                } else {
                  pandora.showToast(Strings.Errors.searchFieldError, context,
                      MessageTypes.WARNING.toString().split('.').last);
                }
              })
        ],
      ),
    );
  }

  void searchAction(String searchQuery, BuildContext context) async {
    Navigator.of(context).pushNamed('/search', arguments: searchQuery);
  }
}
