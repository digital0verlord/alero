import 'package:alero/models/customer/TouchPointData.dart';
import 'package:alero/network/AleroAPIService.dart';
import 'package:fl_chart/fl_chart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_shimmer_widget/flutter_shimmer_loading_widget.dart';
import 'package:flutter_svg/svg.dart';
import '../../../style/theme.dart' as Style;
import 'package:async/async.dart';

import 'empty_list_item.dart';
import 'indicator.dart';

class CustomerTouchPointBody extends StatefulWidget {
  final String customerId, groupId;

  const CustomerTouchPointBody(
      {Key key, @required this.customerId, @required this.groupId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return _CustomerTouchPointBodyState();
  }
}

class _CustomerTouchPointBodyState extends State<CustomerTouchPointBody> {
  bool isTouchPointValue = false;
  var apiService = AleroAPIService();
  int touchedIndex;
  final AsyncMemoizer _asyncMemoizer = AsyncMemoizer();

  List<TouchPointData> tpData = [];
  List<Color> randomColors = [];
  List<Widget> indicators = [];

  bool loading = true;
  bool hasdata;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(left: 20, right: 20, top: 30),
      child: Column(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Expanded(
                child: Row(
                  children: [
                    Text('Touchpoint',
                        style: TextStyle(
                          color: Style.Colors.blackTextColor,
                          fontSize: 12.0,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Poppins-Bold',
                        )),
                  ],
                ),
              ),
              InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Align(
                  alignment: Alignment.centerRight,
                  child: SvgPicture.asset(
                    'assets/customer/dialog_close_button.svg',
                  ),
                ),
              ),
            ],
          ),
          Row(
            children: [
              Text('Volume',
                  style: TextStyle(
                    color: Style.Colors.blackTextColor,
                    fontSize: 8.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins-Bold',
                  )),
              Switch(
                value: isTouchPointValue,
                onChanged: toggleSpendData,
                activeColor: Style.Colors.overviewActiveBg,
                activeTrackColor: Style.Colors.blackTextColor,
                inactiveThumbColor: Colors.white,
                inactiveTrackColor: Colors.grey,
              ),
              Text('Value',
                  style: TextStyle(
                    color: Style.Colors.blackTextColor,
                    fontSize: 8.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: 'Poppins-Bold',
                  )),
            ],
          ),
          SizedBox(
            height: 12,
          ),
          loadCountData()
        ],
      ),
    );
  }

  void toggleSpendData(bool value) {
    if (mounted) {
      setState(() {
        isTouchPointValue = value;
        getTouchPoints(widget.groupId);
      });
    }
  }

  Future getTouchPoints(String groupId) async {
    return this._asyncMemoizer.runOnce(() async {
      var lifeStyle = await apiService.getTouchPointData(groupId);
      tpData = [];
      List<Widget> _indicators = [];
      if (lifeStyle.length == 0) {
        if (mounted) {
          setState(() {
            hasdata = false;
          });
        }
      } else {
        if (mounted) {
          setState(() {
            hasdata = true;
          });
        }
        lifeStyle.forEach((counter) {
          tpData.add(TouchPointData(
              channel: counter["channel"],
              averageSpend: counter["averageSpend"],
              volumeSpend: counter["volumeSpend"],
              transactionChannelCount: counter["transactionChannelCount"]));
        });
      }
      generateColors(tpData.length);
      for (var i = 0; i < lifeStyle.length; i++) {
        _indicators.add(Indicator(
          color: randomColors[i],
          text: tpData[i].channel,
          isSquare: false,
        ));
      }
      if (mounted) {
        setState(() {
          indicators = _indicators;
        });
      }
      return lifeStyle;
    });
  }

  Widget loadCountData() {
    return FutureBuilder(
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.none &&
                snapshot.hasData == null ||
            snapshot.connectionState == ConnectionState.waiting) {
          return FlutterShimmnerLoadingWidget(
            count: 2,
            animate: true,
            color: Colors.grey[200],
          );
        }
        if (hasdata) {
          return Expanded(
            child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                child: AspectRatio(
                  aspectRatio: 1,
                  child: Column(
                    children: <Widget>[
                      const SizedBox(
                        height: 18,
                      ),
                      Expanded(
                        child: AspectRatio(
                          aspectRatio: 1,
                          child: PieChart(
                            PieChartData(
                                pieTouchData: PieTouchData(
                                    touchCallback: (pieTouchResponse) {
                                  if (mounted) {
                                    setState(() {
                                      if (pieTouchResponse.touchInput
                                              is FlLongPressEnd ||
                                          pieTouchResponse.touchInput
                                              is FlPanEnd) {
                                        touchedIndex = -1;
                                      } else {
                                        touchedIndex = pieTouchResponse
                                            .touchedSectionIndex;
                                      }
                                    });
                                  }
                                }),
                                borderData: FlBorderData(
                                  show: false,
                                ),
                                sectionsSpace: 0,
                                centerSpaceRadius: 50,
                                sections: showingTouchPointSections()),
                          ),
                        ),
                      ),
                      const SizedBox(
                        height: 40,
                      ),
                      Expanded(
                        child: GridView.count(
                          crossAxisCount: 2,
                          childAspectRatio: 4.0,
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          children: indicators,
                        ),
                      ),
                      const SizedBox(
                        width: 8,
                      ),
                    ],
                  ),
                )),
          );
        } else {
          return EmptyListItem(
            message: 'No Customer Touch Points',
          );
        }
      },
      future: getTouchPoints(widget.groupId),
    );
  }

  List<PieChartSectionData> showingTouchPointSections() {
    if (tpData.isNotEmpty)
      return List.generate(tpData.length, (i) {
        final isTouched = i == touchedIndex;
        final double fontSize = isTouched ? 25 : 16;
        final double radius = isTouched ? 60 : 50;
        return PieChartSectionData(
            color: randomColors[i],
            value: (!isTouchPointValue)
                ? tpData[i].volumeSpend
                : tpData[i].transactionChannelCount.toDouble(),
            title: '',
            radius: radius,
            titleStyle: TextStyle(
              fontSize: fontSize,
              fontWeight: FontWeight.bold,
              color: randomColors[i],
            ));
      });
  }

  void generateColors(int length) {
    var list = [
      0xFF99C9D9,
      0xFF555555,
      0xFF008EC4,
      0xFFBBBBBB,
      0xFFFFDAA6,
      0xFFB3A369,
      0xFFF4B459,
      0xFF7AC369
    ];
    for (int i = 0; i < length; i++) {
      if (!randomColors.contains(list[i])) {
        randomColors.add(Color(list[i]));
      }
    }
  }
}
