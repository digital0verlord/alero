import 'package:alero/models/customer/TransactionFlow.dart';
import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import 'package:intl/intl.dart';

class LineChartInflowOutFlow extends StatefulWidget {
  final List<TransactionFlow> tfData;
  final Color color;

  const LineChartInflowOutFlow(
      {Key key, @required this.tfData, @required this.color})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => LineChartInflowOutFlowState();
}

class LineChartInflowOutFlowState extends State<LineChartInflowOutFlow> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AspectRatio(
      aspectRatio: 3,
      child: Card(
          elevation: 0,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4)),
          color: Colors.white,
          child: loadLineChart()),
    );
  }

  Widget loadLineChart() {
    return Center(
        child: Container(
            child: SfCartesianChart(
                // Initialize category axis
                primaryXAxis: CategoryAxis(),
                primaryYAxis: NumericAxis(
                    //Formatting the labels in locale’s currency pattern with symbol.
                    numberFormat: NumberFormat.compactCurrency(
                        decimalDigits: 1, symbol: "₦")),
                tooltipBehavior:
                    TooltipBehavior(enable: true, header: 'Amount'),
                series: <LineSeries<TransactionFlow, String>>[
          LineSeries<TransactionFlow, String>(
            // Bind data source
            dataSource: widget.tfData,
            xValueMapper: (TransactionFlow tFlow, _) =>
                tFlow.transactionDate.substring(0, 3),
            yValueMapper: (TransactionFlow tFlow, _) => tFlow.totalSpend,
            color: widget.color,
          )
        ])));
  }
}
