import 'dart:async';

class Global {
  static String BaseUrl = 'https://utrack.unionbankng.com/api';
  //static String BaseUrl = 'https://172.16.11.179:4443/api';
  static String API_TOKEN = '';
  static String PREF_TOKEN = '';

  static bool isLoggedIn = false;
  static String USER_NAME;
  static bool isRM = false;
  static var STAFF_INFORMATION = null;
  static String AppId;
  static String DeviceIp;
  static String DeviceManufacturer;
  static String DeviceName;
  static String DeviceType;
  static String DeviceModel;
  static int APP_TIMEOUT = 10;
  static Timer timer;
}
